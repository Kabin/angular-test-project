import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutUsComponent } from './components/about-us/about-us.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
  {path: 'about', component:AboutUsComponent},
  //lazyloading createstudent component
  {
    path: 'createStudent',  
     loadChildren: () => import('./module/create-student/create-student.module')  
     .then(m => m.CreateStudentModule) 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
