import{ Injectable} from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpInterceptorService } from '../interceptorService/interceptorservice';


@Injectable()
export class CreateStudentService extends HttpInterceptorService{


    //saving batch and student 
    // here we are calling api
    //api, controllername and method
  private readonly _createStudentBatch: string = "/api/CreatestudentBatch/CreateBatch"; 
  private readonly _createStudentDetails: string = "/api/CreatestudentBatch/CreateStudent"; 
  get createStudentBatchUrl(){return this.baseUrl + this._createStudentBatch};

    constructor(private httpClient: HttpClient){
        super()
    }

    getAllStudentsDetails() {
        return this.httpClient.get('./assets/studentDetails.json');
    }

    createBatch(batchViewModel) {
        return this.httpClient.post(this._createStudentBatch, batchViewModel);
    }

    createStudentDetails(studentDetailsViewmodel){
        return this.httpClient.post(this._createStudentDetails, studentDetailsViewmodel);
    }

}