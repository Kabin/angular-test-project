
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
    public baseUrl: string = 'http://localhost:50501';
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        const token = '123123-123OKNGhji-uik569874-1Klpokfxcg' //demo token
        const reqToken = req.clone({ 
            setHeaders: {
                Authorization:`Bearer ${token}`
            } 
          });
    return next.handle(reqToken)
    .pipe(
        retry(2),
        // here we handle error
        catchError((error: HttpErrorResponse) => {
            let errorMsg = '';
            //TODO: need to add error handling logic here
            if (error.error instanceof ErrorEvent) {
                alert('error occured!');
                errorMsg = `Error: ${error.error.message}`;
              }
            alert('oops, Error occured!');
            return throwError(error);
        })  
    )

    }  

}