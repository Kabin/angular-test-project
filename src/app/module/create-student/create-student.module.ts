import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CreateNewStudentComponent } from 'src/app/components/create-new-student/create-new-student.component';
import { CreateStudentService } from 'src/app/services/createStudent.service';
import { FormsModule } from '@angular/forms';

// this is modularization design pattern
const routes: Routes = [
  { path: '', component: CreateNewStudentComponent },
];
@NgModule({
  declarations: [CreateNewStudentComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
    
  ],
  providers:[CreateStudentService]
})
export class CreateStudentModule { }
