import { Component, OnInit } from '@angular/core';
import { CreateStudentService } from 'src/app/services/createStudent.service';

@Component({
  selector: 'app-create-new-student',
  templateUrl: './create-new-student.component.html',
  styleUrls: ['./create-new-student.component.css']
})
export class CreateNewStudentComponent implements OnInit {
  public tryToSave = false;
 public studentsDetailsList:any[] = [];
 public index: number;
 public studentLbl = 'Add'
 public studentBatch = {}; /// need to assign viewmodal for now assigning object
 public student: any = {studentName: '', studentId: '', Gender: '', phoneNumber: ''};  ///need to creat viewmodel
  constructor(private createStudentService: CreateStudentService) { }

  ngOnInit() {
    this.getAllStudentsDetails();
  }

  getAllStudentsDetails(){
    this.createStudentService.getAllStudentsDetails().subscribe((result:any) => {
      // this.studentsDetailsList = result.students;
      this.studentsDetailsList.push(result);
    })
  }
  showStudentDetails(student, index) {
    if(this.index === index) {
      this.index = -1;
    } else{
      this.index = index;

    }  
  }

  addBatch(){
    this.createStudentService.createBatch(this.studentBatch).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    })
  }

  addStudent() {
    this.tryToSave = true;

    this.createStudentService.createStudentDetails(this.student).subscribe(res =>{
      console.log(res)
    }, err => console.log(err));
  }

  updateStudent(stu) {
    this.student = stu;
  }

  validateNumber(e) {
    this.student.studentName = this.student.studentName.replace(/[0-9]/g, '');
    return this.student;
  }

  checkLength(number) {
    this.student.phoneNumber = this.student.phoneNumber.toString().replace(/[a-zA-Z]/g, '');

  }
  delete(i) {
    this.studentsDetailsList.forEach(res => {
      res.students.splice(i, 1);
    })
  }
}
